#!/usr/bin/env bash

set -e

cleanup() {
	rm -f /tmp/hemleb.de.ca.crt /tmp/hemleb.de.client.crt /tmp/hemleb.de.client.key
}

trap 'cleanup' EXIT

ROOT=$( cd "$(dirname "${BASH_SOURCE[0]}")/../" ; pwd -P )

CLUSTER_NAME="hemleb.de"

HOST=$("${ROOT}/ansible/.venv/bin/ansible-inventory" -i "${ROOT}/ansible/inventory" --list | jq -r ".ungrouped.hosts|first")

API_URL="https://${HOST}:6443"

export $(ssh -q "ansible@${HOST}" "sudo cat /etc/rancher/k3s/k3s.yaml" \
	| yq eval -j - \
	| jq -r '{hemleb_ca: .clusters[0].cluster["certificate-authority-data"]}, {hemleb_client_crt: .users[0].user["client-certificate-data"]}, {hemleb_client_key: .users[0].user["client-key-data"]}
		| to_entries[]
		| "\(.key | ascii_upcase)=\(.value)"')

echo $HEMLEB_CA | base64 -d > /tmp/hemleb.de.ca.crt
echo $HEMLEB_CLIENT_CRT | base64 -d > /tmp/hemleb.de.client.crt
echo $HEMLEB_CLIENT_KEY | base64 -d > /tmp/hemleb.de.client.key

kubectl config set-cluster "${CLUSTER_NAME}" --embed-certs \
	--server=$API_URL \
	--certificate-authority=/tmp/hemleb.de.ca.crt

kubectl config set-credentials "${CLUSTER_NAME}" --embed-certs \
	--client-certificate=/tmp/hemleb.de.client.crt \
	--client-key=/tmp/hemleb.de.client.key

kubectl config set-context "${CLUSTER_NAME}" --cluster="${CLUSTER_NAME}" --user="${CLUSTER_NAME}"

cleanup

echo
echo 'Success! Cluster "'"${CLUSTER_NAME}"'" configured.'
